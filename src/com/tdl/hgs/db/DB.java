package com.tdl.hgs.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.tdl.hgs.modal.GlobalCase;
import com.tdl.hgs.modal.MsgContent;


public class DB {

	/** The con. */
	Connection con = null;
	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(DB.class);
	
	public boolean openCon(){
		String dbDriver="com.mysql.jdbc.Driver";
		String dbUrl="jdbc:mysql://localhost:3306/globalvodaphone";
		String dbUserid="root";
		String dbPassword="";
		//String JNDI ="java:comp/env/jdbc/globalvodaphone";
		return getConnection(dbDriver, dbUrl, dbUserid, dbPassword);
	}
	private boolean  getConnection(String dbDriver,String dbUrl,String dbUserid,String dbPassword){	
		try {
			
			Class.forName(dbDriver);
            
		  }catch(java.lang.ClassNotFoundException e) {
				logger.debug("Class Not Found Exception :");
				logger.debug(e.getMessage());
				return false;
		 }catch(Exception e) {
				logger.debug("Class Not Found Exception :");
				e.printStackTrace();	
				logger.debug(e.getMessage());
				return false;
		 }
		
		try {
		      // Setup the connection with the DB
			con = DriverManager.getConnection(dbUrl,dbUserid,dbPassword);
			} catch(SQLException ex) {
				
				ex.printStackTrace();
				logger.debug("SQLException: " + ex.getMessage());
				return false;
			}	
	  
		return true;
	}
	public boolean closeCon() {
		
			try {
				if(con!=null)
					con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return true;
	}
	public boolean insert2DB(ArrayList<GlobalCase> list){
		System.out.println("In insert...................");
		int result =0;
		PreparedStatement pstmtGS = null,pstmtMSG = null;
		try {
			
			String ADD_GC ="INSERT INTO chatGlobalCase (nGlobalCaseID , CustName , nWaitTime , "
					+ "dtSessionStartTime , dtEndTime , AgentName , message , sessionTime , "
					+ "CustKeywords , AgentKeywords , Postpaid , Prepaid , Sentiment,IP,Location) "
					+ "VALUES(?,?,?,?,? ,?,?,?,?,?,?,?,?,?,?)";
			String ADD_MSG_CONTENT = "INSERT INTO chatMsgContent (nGlobalCaseID, msgTime, speaker,"
					+ "msg,speakerName,msgKeywords,QRT) VALUES(?,?,?,?,?,?,?)";
			pstmtGS = con.prepareStatement(ADD_GC);
			pstmtMSG = con.prepareStatement(ADD_MSG_CONTENT);
			for (int i = 0; i < list.size(); i++) {
				//System.out.println(i);
				GlobalCase gs = list.get(i);
				pstmtGS.setInt(1, gs.getnGlobalCaseID());
				pstmtGS.setString(2, gs.getCustName());
				pstmtGS.setInt(3, gs.getnWaitTime());
				pstmtGS.setDate(4,  new java.sql.Date(gs.getDtSessionStartTime().getTime()));
				pstmtGS.setDate(5, new java.sql.Date(gs.getDtEndTime().getTime()));
				pstmtGS.setString(6, gs.getAgentName());
				pstmtGS.setString(7, gs.getMessage());
				pstmtGS.setInt(8, gs.getSessionTime());
				pstmtGS.setString(9, gs.getCustKeywords());
				pstmtGS.setString(10, gs.getAgentKeywords());
				pstmtGS.setInt(11, gs.getPostpaid());
				pstmtGS.setInt(12, gs.getPrepaid());
				pstmtGS.setInt(13, gs.getSentiment());
				pstmtGS.setString(14, gs.getIP());
				pstmtGS.setString(15, gs.getLocation());
				pstmtGS.addBatch();
				//8/1/2015 8:00:59 AM
				//mm/dd/yyyy hh:mm:ss 
				ArrayList<MsgContent> listMessages = gs.getListMessages();
				for (int j = 0; j < listMessages.size(); j++) {
					//System.out.println("j = %d"+j);
					MsgContent msg = listMessages.get(j);
					pstmtMSG.setInt(1, msg.getnGlobalCaseID());
					pstmtMSG.setDate(2, new java.sql.Date(msg.getMsgTime().getTime()));
					pstmtMSG.setString(3, msg.getSpeaker());
					pstmtMSG.setString(4, msg.getMsg());
					pstmtMSG.setString(5, msg.getSpeakerName());
					pstmtMSG.setString(6, msg.getMsgKeywords());
					pstmtMSG.setInt(7, msg.getQRT());
					pstmtMSG.addBatch();
				}
			}
			// Batch is ready, execute it to insert the data

            pstmtGS.executeBatch();
            pstmtMSG.executeBatch();
			if (pstmtGS != null)
				pstmtGS.close();
			if (pstmtMSG != null)
				pstmtMSG.close();
		} catch (SQLException sql) {
			logger.debug("SQLException " + sql);
			try {
				if (pstmtGS != null)
					pstmtGS.close();
				if (pstmtMSG != null)
					pstmtMSG.close();
				return false;
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
				return false;
			}
		} catch (Exception exe) {
			logger.debug("Exception " + exe);
			try {
				if (pstmtGS != null)
					pstmtGS.close();
				if (pstmtMSG != null)
					pstmtMSG.close();
				return false;
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
				return false;
			}
		} finally {
			try {
				if (pstmtGS != null)
					pstmtGS.close();
				if (pstmtMSG != null)
					pstmtMSG.close();
			} catch (SQLException sql) {
				logger.debug("SQLException " + sql);
			}
		}
		if(result==1)
			return true;
		else 
			return false;
	}
	
	public int getNumberOfChatSessions(){
		logger.debug("In getNumberOfChatSessions() Method...");
		int totalChatSession =0;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			
			String query ="select count(*) from globalvodaphone.chatglobalcase";
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while(rs.next()){
				totalChatSession = rs.getInt(1);
			}
			if (stmt != null)
				stmt.close();
			if (rs != null)
				rs.close();
		} catch (SQLException sql) {
			logger.debug("SQLException " + sql);
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalChatSession;
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
				return totalChatSession;
			}
		} catch (Exception exe) {
			logger.debug("Exception " + exe);
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalChatSession;
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
				return totalChatSession;
			}
		} finally {
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalChatSession;
			} catch (SQLException sql) {
				logger.debug("SQLException " + sql);
			}
		}
		return totalChatSession;
	}
	
	public int getTotalNumberOfRecords(){
		logger.debug("In getTotalNumberOfRecords() Method...");
		int totalNumOfRecs =0;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			
			String query ="select count(*) from chatmsgcontent";
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while(rs.next()){
				totalNumOfRecs = rs.getInt(1);
			}
			if (stmt != null)
				stmt.close();
			if (rs != null)
				rs.close();
		} catch (SQLException sql) {
			logger.debug("SQLException " + sql);
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
				return totalNumOfRecs;
			}
		} catch (Exception exe) {
			logger.debug("Exception " + exe);
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
				return totalNumOfRecs;
			}
		} finally {
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql) {
				logger.debug("SQLException " + sql);
			}
		}
		return totalNumOfRecs;
	}
	
	public int getTotalPrepaidChatSession(){
		logger.debug("In getTotalPrepaidChatSession() Method...");
		int totalNumOfRecs =0;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			
			String query ="SELECT count(*) FROM chatglobalcase where Prepaid=1";
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while(rs.next()){
				totalNumOfRecs = rs.getInt(1);
			}
			if (stmt != null)
				stmt.close();
			if (rs != null)
				rs.close();
		} catch (SQLException sql) {
			logger.debug("SQLException " + sql);
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
				return totalNumOfRecs;
			}
		} catch (Exception exe) {
			logger.debug("Exception " + exe);
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
				return totalNumOfRecs;
			}
		} finally {
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql) {
				logger.debug("SQLException " + sql);
			}
		}
		return totalNumOfRecs;
	}
	
	public int getTotalPostpaidChatSession(){
		logger.debug("In getTotalPostpaidChatSession() Method...");
		int totalNumOfRecs =0;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			
			String query ="SELECT count(*) FROM globalvodaphone.chatglobalcase where postpaid=1";
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while(rs.next()){
				totalNumOfRecs = rs.getInt(1);
			}
			if (stmt != null)
				stmt.close();
			if (rs != null)
				rs.close();
		} catch (SQLException sql) {
			logger.debug("SQLException " + sql);
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
				return totalNumOfRecs;
			}
		} catch (Exception exe) {
			logger.debug("Exception " + exe);
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
				return totalNumOfRecs;
			}
		} finally {
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql) {
				logger.debug("SQLException " + sql);
			}
		}
		return totalNumOfRecs;
	}

	public int getTotalUniqueAgents(){
		logger.debug("In getTotalUniqueAgents() Method...");
		int totalNumOfRecs =0;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			
			String query ="SELECT count(distinct AgentName) FROM chatglobalcase;";
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while(rs.next()){
				totalNumOfRecs = rs.getInt(1);
			}
			
			if (stmt != null)
				stmt.close();
			if (rs != null)
				rs.close();
		} catch (SQLException sql) {
			logger.debug("SQLException " + sql);
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
				return totalNumOfRecs;
			}
		} catch (Exception exe) {
			logger.debug("Exception " + exe);
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
				return totalNumOfRecs;
			}
		} finally {
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql) {
				logger.debug("SQLException " + sql);
			}
		}
		return totalNumOfRecs;
	}
	
	public float getAvgHandleTime(){
		logger.debug("In getAvgHandleTime() Method...");
		float totalNumOfRecs =0;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			
			String query ="select sum(sessionTime)/(SELECT count(*) FROM chatglobalcase) FROM globalvodaphone.chatglobalcase";
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while(rs.next()){
				totalNumOfRecs = rs.getFloat(1);
			}
			if (stmt != null)
				stmt.close();
			if (rs != null)
				rs.close();
		} catch (SQLException sql) {
			logger.debug("SQLException " + sql);
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
				return totalNumOfRecs;
			}
		} catch (Exception exe) {
			logger.debug("Exception " + exe);
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
				return totalNumOfRecs;
			}
		} finally {
			try {
				if(con!=null)
					con.close();
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
				return totalNumOfRecs;
			} catch (SQLException sql) {
				logger.debug("SQLException " + sql);
			}
		}
		return totalNumOfRecs;
	}
	
	public List<GlobalCase> getGlobalCaseList(){
		return getGlobalCaseList(null);
	}
	//getGlobalCaseList
	public List<GlobalCase> getGlobalCaseList(String location){
		logger.debug("In getGlobalCaseList() Method...");
		Statement stmt = null;
		ResultSet rs = null;
		GlobalCase globalCase = null;
		ArrayList<GlobalCase> globalCaseList = new ArrayList<GlobalCase>();
		ArrayList<GlobalCase> mapGlobalCaseList=null;
		Map<String,ArrayList<GlobalCase>> agentWiseMap = new HashMap<String,ArrayList<GlobalCase>>();
		Map<String,ArrayList<GlobalCase>> locationWiseMap = new HashMap<String,ArrayList<GlobalCase>>();
		try {
			
			String query ="select nGlobalCaseID,CustName,nWaitTime,dtSessionStartTime,dtEndTime,AgentName,message,sessionTime,agentKeyWords,Postpaid, Prepaid, Sentiment, CustKeywords,Ip, Location from chatglobalcase";
			if(location!=null){
				query+= " where location = '"+ location + "' ";
			}
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			Set<String> uniqueAgent = new HashSet<String>();
			Set<String> uniqueLocation = new HashSet<String>();
			//List<GlobalCase> globalCaseList = new ArrayList<GlobalCase>();
			while(rs.next()){
				globalCase = new GlobalCase();
				mapGlobalCaseList = new ArrayList<GlobalCase>();
				globalCase.setnGlobalCaseID(rs.getInt(1));
				globalCase.setCustName(rs.getString(2));
				globalCase.setnWaitTime(rs.getInt(3));
				globalCase.setDtSessionStartTime(rs.getDate(4));
				globalCase.setDtEndTime(rs.getDate(5));
				globalCase.setAgentName(rs.getString(6));
				globalCase.setMessage(rs.getString(7));
				globalCase.setSessionTime(rs.getInt(8));
				globalCase.setAgentKeywords(rs.getString(9));
				globalCase.setPostpaid(rs.getInt(10));
				globalCase.setPrepaid(rs.getInt(11));
				globalCase.setSentiment(rs.getInt(12));
				globalCase.setCustKeywords(rs.getString(13));
				globalCase.setIP(rs.getString(14));
				globalCase.setLocation(rs.getString(15));
				
				uniqueAgent.add(rs.getString(6));
				uniqueLocation.add(rs.getString(15));
				globalCase.setUniqueAgent(uniqueAgent);
				globalCase.setUniqueLocation(uniqueLocation);
				mapGlobalCaseList.add(globalCase);
				//boolean flag=false;
				// Agent wise map
				if(agentWiseMap.containsKey(globalCase.getAgentName())){

					ArrayList<GlobalCase> glCase = agentWiseMap.get(globalCase.getAgentName().trim());
					glCase.add(globalCase);
					mapGlobalCaseList.add(globalCase);
					agentWiseMap.put(globalCase.getAgentName(), glCase);
				}
				else{
					//globalCaseList.add(globalCase);
					agentWiseMap.put(globalCase.getAgentName().trim(), mapGlobalCaseList);
					
					//flag=true;
				}
				
				// location wise map
				if(locationWiseMap.containsKey(globalCase.getLocation())){

					ArrayList<GlobalCase> glCaseLoc = locationWiseMap.get(globalCase.getLocation().trim());
					glCaseLoc.add(globalCase);
					mapGlobalCaseList.add(globalCase);
					locationWiseMap.put(globalCase.getLocation(), glCaseLoc);
				}
				else{
					//globalCaseList.add(globalCase);
					locationWiseMap.put(globalCase.getLocation().trim(), mapGlobalCaseList);
					
					//flag=true;
				}

				globalCase.setAgentWiseMap(agentWiseMap);
				globalCase.setLocationWiseMap(locationWiseMap);
				globalCaseList.add(globalCase);
				//agentWiseMap.put(globalCase.getAgentName(),globalCaseList);
				
			}
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		} catch (SQLException sql) {
			logger.debug("SQLException " + sql);
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
		
			}
		} catch (Exception exe) {
			logger.debug("Exception " + exe);
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql1) {
				logger.debug("SQLException " + sql1);
		
			}
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.debug("SQLException " + sql);
			}
		}
		return globalCaseList;
	}
	
	// Chat Message Content
	//getGlobalCaseList
		public List<MsgContent> getChatMsgContentList(DB db){
			db.openCon();
			logger.debug("In getChatMsgContentList() Method...");
			Statement stmt = null;
			ResultSet rs = null;
			MsgContent chat = null;
			ArrayList<MsgContent> chatList = new ArrayList<MsgContent>();
			Map<String,ArrayList<MsgContent>> agentWiseChatMap = new HashMap<String,ArrayList<MsgContent>>();
			try {
				
				String query ="select nGlobalCaseID,msgTime,speaker,msg,MsgKeywords,SpeakerName,QRT from chatmsgcontent";
				stmt = con.createStatement();
				rs = stmt.executeQuery(query);
				Set<String> uniqueAgent = new HashSet<String>();
				ArrayList<MsgContent> msgContentList = null;
				while(rs.next()){
					msgContentList =  new ArrayList<MsgContent>();
					chat = new MsgContent();
					chat.setnGlobalCaseID(rs.getInt(1));
					chat.setMsgTime(rs.getDate(2));
					chat.setSpeaker(rs.getString(3));
					chat.setMsg(rs.getString(4));
					chat.setMsgKeywords(rs.getString(5));
					chat.setSpeakerName(rs.getString(6));
					chat.setQRT(rs.getInt(7));
					
					uniqueAgent.add(rs.getString(3));
					chat.setUniqueSpeaker(rs.getString(3));
					msgContentList.add(chat);
					if(agentWiseChatMap.containsKey(chat.getSpeakerName().trim())){

						ArrayList<MsgContent> msgList = agentWiseChatMap.get(chat.getSpeakerName().trim());
						msgList.add(chat);
						msgContentList.add(chat);
						agentWiseChatMap.put(chat.getSpeakerName().trim(), msgList);
					}
					else{

						agentWiseChatMap.put(chat.getSpeakerName().trim(), msgContentList);
						
					}
				
					chat.setAgentWiseChatMap(agentWiseChatMap);

					
					chatList.add(chat);
										
				}
				if (stmt != null)
					stmt.close();
				if (rs != null)
					rs.close();
			} catch (SQLException sql) {
				logger.debug("SQLException " + sql);
				try {
					if(con!=null)
						con.close();
					if (stmt != null)
						stmt.close();
					if (rs != null)
						rs.close();
			
				} catch (SQLException sql1) {
					logger.debug("SQLException " + sql1);
			
				}
			} catch (Exception exe) {
				logger.debug("Exception " + exe);
				try {
					if(con!=null)
						con.close();
					if (stmt != null)
						stmt.close();
					if (rs != null)
						rs.close();
			
				} catch (SQLException sql1) {
					logger.debug("SQLException " + sql1);
			
				}
			} finally {
				try {
					if(con!=null)
						con.close();
					if (stmt != null)
						stmt.close();
					if (rs != null)
						rs.close();
			
				} catch (SQLException sql) {
					logger.debug("SQLException " + sql);
				}
			}
			return chatList;
	}

		public List<String> getUniqueLocation(){
			logger.debug("In getUniqueLocation() Method...");
			Statement stmt = null;
			ResultSet rs = null;
			List<String> locationList = new ArrayList<String>();
			try {
				String query ="SELECT distinct location FROM globalvodaphone.chatglobalcase where Sentiment<0";
				stmt = con.createStatement();
				rs = stmt.executeQuery(query);
				while(rs.next()){
					locationList.add(rs.getString(1));
				}
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			} catch (SQLException sql) {
				logger.debug("SQLException " + sql);
				try {
					if (rs != null)
						rs.close();
					if (stmt != null)
						stmt.close();
				} catch (SQLException sql1) {
					logger.debug("SQLException " + sql1);
			
				}
			} catch (Exception exe) {
				logger.debug("Exception " + exe);
				try {
					if (rs != null)
						rs.close();
					if (stmt != null)
						stmt.close();
				} catch (SQLException sql1) {
					logger.debug("SQLException " + sql1);
			
				}
			} finally {
				try {
					if (rs != null)
						rs.close();
					if (stmt != null)
						stmt.close();
				} catch (SQLException sql) {
					logger.debug("SQLException " + sql);
				}
			}
			return locationList;
	}
}

