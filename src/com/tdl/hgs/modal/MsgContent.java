package com.tdl.hgs.modal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MsgContent {
	int nGlobalCaseID;
	Date msgTime;
	String speaker;
	String msg;
	String speakerName;
	String msgKeywords;
	Map<String,ArrayList<MsgContent>> agentWiseChatMap = new HashMap<String,ArrayList<MsgContent>>();
	
	public Map<String, ArrayList<MsgContent>> getAgentWiseChatMap() {
		return agentWiseChatMap;
	}
	public void setAgentWiseChatMap(
			Map<String, ArrayList<MsgContent>> agentWiseChatMap) {
		this.agentWiseChatMap = agentWiseChatMap;
	}
	public String getUniqueSpeaker() {
		return uniqueSpeaker;
	}
	public void setUniqueSpeaker(String uniqueSpeaker) {
		this.uniqueSpeaker = uniqueSpeaker;
	}
	int QRT;
	String uniqueSpeaker;
	public int getnGlobalCaseID() {
		return nGlobalCaseID;
	}
	public void setnGlobalCaseID(int nGlobalCaseID) {
		this.nGlobalCaseID = nGlobalCaseID;
	}
	public Date getMsgTime() {
		return msgTime;
	}
	public void setMsgTime(Date msgTime) {
		this.msgTime = msgTime;
	}
	public String getSpeaker() {
		return speaker;
	}
	public void setSpeaker(String speaker) {
		this.speaker = speaker;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getSpeakerName() {
		return speakerName;
	}
	public void setSpeakerName(String speakerName) {
		this.speakerName = speakerName;
	}
	public String getMsgKeywords() {
		return msgKeywords;
	}
	public void setMsgKeywords(String msgKeywords) {
		this.msgKeywords = msgKeywords;
	}
	public int getQRT() {
		return QRT;
	}
	public void setQRT(int qRT) {
		QRT = qRT;
	}
	@Override
	public String toString() {
		return "MsgContent [nGlobalCaseID=" + nGlobalCaseID + ", msgTime=" + msgTime + ", speaker=" + speaker + ", msg="
				+ msg + ", speakerName=" + speakerName + ", msgKeywords=" + msgKeywords + ", QRT=" + QRT + "]";
	}
	
}
