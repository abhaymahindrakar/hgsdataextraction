package com.tdl.hgs.modal;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GlobalCase {
	int nGlobalCaseID;
	String CustName = "";
	int nWaitTime;
	Date dtSessionStartTime;
	Date dtEndTime;
	int sessionTime;
	String AgentName;
	String message;
	String IP;
	String location;
	String CustKeywords;
	String AgentKeywords;
	int Postpaid = 0;
	int Prepaid = 0;
	int Sentiment = 0;
	Set<String> uniqueLocation;
	
	
	int noOfChatSession=0;
	float aht=0.0f;
	
	public int getNoOfChatSession() {
		return noOfChatSession;
	}
	public void setNoOfChatSession(int noOfChatSession) {
		this.noOfChatSession = noOfChatSession;
	}
	public float getAht() {
		return aht;
	}
	public void setAht(float aht) {
		this.aht = aht;
	}
Map<String,ArrayList<GlobalCase>> bucketListMap = new HashMap<String,ArrayList<GlobalCase>>();
	
	
	public Map<String, ArrayList<GlobalCase>> getBucketListMap() {
	return bucketListMap;
}
public void setBucketListMap(Map<String, ArrayList<GlobalCase>> bucketListMap) {
	this.bucketListMap = bucketListMap;
}
	Map<String,Integer> bucketMap = new HashMap<String,Integer>();
	public Map<String, Integer> getBucketMap() {
		return bucketMap;
	}
	public void setBucketMap(Map<String, Integer> bucketMap) {
		this.bucketMap = bucketMap;
	}
	Set<String> uniqueAgent = new HashSet<String>();
	Map<String,ArrayList<GlobalCase>> agentWiseMap = new HashMap<String,ArrayList<GlobalCase>>();
	Map<String,ArrayList<GlobalCase>> locationWiseMap = new HashMap<String,ArrayList<GlobalCase>>();
	public Map<String, ArrayList<GlobalCase>> getAgentWiseMap() {
		return agentWiseMap;
	}
	public void setAgentWiseMap(Map<String, ArrayList<GlobalCase>> agentWiseMap) {
		this.agentWiseMap = agentWiseMap;
	}
	public Set<String> getUniqueAgent() {
		return uniqueAgent;
	}
	public void setUniqueAgent(Set<String> uniqueAgent) {
		this.uniqueAgent = uniqueAgent;
	}
	ArrayList<MsgContent> listMessages;
	public int getnGlobalCaseID() {
		return nGlobalCaseID;
	}
	public void setnGlobalCaseID(int nGlobalCaseID) {
		this.nGlobalCaseID = nGlobalCaseID;
	}
	public String getCustName() {
		return CustName;
	}
	public void setCustName(String custName) {
		CustName = custName;
	}
	public int getnWaitTime() {
		return nWaitTime;
	}
	public void setnWaitTime(int nWaitTime) {
		this.nWaitTime = nWaitTime;
	}
	public Date getDtSessionStartTime() {
		return dtSessionStartTime;
	}
	public void setDtSessionStartTime(Date dtSessionStartTime) {
		this.dtSessionStartTime = dtSessionStartTime;
	}
	public Date getDtEndTime() {
		return dtEndTime;
	}
	public void setDtEndTime(Date dtEndTime) {
		this.dtEndTime = dtEndTime;
	}
	public int getSessionTime() {
		return sessionTime;
	}
	public void setSessionTime(int sessionTime) {
		this.sessionTime = sessionTime;
	}
	public String getAgentName() {
		return AgentName;
	}
	public Set<String> getUniqueLocation() {
		return uniqueLocation;
	}
	public void setUniqueLocation(Set<String> uniqueLocation) {
		this.uniqueLocation = uniqueLocation;
	}
	public void setAgentName(String agentName) {
		AgentName = agentName;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCustKeywords() {
		return CustKeywords;
	}
	public void setCustKeywords(String custKeywords) {
		CustKeywords = custKeywords;
	}
	public String getAgentKeywords() {
		return AgentKeywords;
	}
	public void setAgentKeywords(String agentKeywords) {
		AgentKeywords = agentKeywords;
	}
	public int getPostpaid() {
		return Postpaid;
	}
	public void setPostpaid(int postpaid) {
		Postpaid = postpaid;
	}
	public Map<String, ArrayList<GlobalCase>> getLocationWiseMap() {
		return locationWiseMap;
	}
	public void setLocationWiseMap(
			Map<String, ArrayList<GlobalCase>> locationWiseMap) {
		this.locationWiseMap = locationWiseMap;
	}
	public int getPrepaid() {
		return Prepaid;
	}
	public void setPrepaid(int prepaid) {
		Prepaid = prepaid;
	}
	public int getSentiment() {
		return Sentiment;
	}
	public void setSentiment(int sentiment) {
		Sentiment = sentiment;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public ArrayList<MsgContent> getListMessages() {
		return listMessages;
	}
	public void setListMessages(ArrayList<MsgContent> listMessages) {
		this.listMessages = listMessages;
	}
	@Override
	public String toString() {
		return "GlobalCase [nGlobalCaseID=" + nGlobalCaseID + ", CustName=" + CustName + ", nWaitTime=" + nWaitTime
				+ ", dtSessionStartTime=" + dtSessionStartTime + ", dtEndTime=" + dtEndTime + ", sessionTime="
				+ sessionTime + ", AgentName=" + AgentName + ", message=" + message + ", CustKeywords=" + CustKeywords
				+ ", AgentKeywords=" + AgentKeywords + ", Postpaid=" + Postpaid + ", Prepaid=" + Prepaid
				+ ", Sentiment=" + Sentiment + ", listMessages=" + listMessages + "]";
	}
	
	
}
