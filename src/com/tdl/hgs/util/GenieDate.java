/*

 * * ************************************

 * * TAXI - CEMPIA                    

 * * Release 1.0                 

 * * TheDecisionLabs Consulting Pvt. Ltd.                         

 * * @author Abhay                    

 * * @created Jul 22, 2015

 * **************************************

 */
package com.tdl.hgs.util;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 * The Class GenieDate.
 * This class is used to get the date and date related formatting
 */
public class GenieDate {
	
	/**
	 * This method will return the given Date in SimpleDateFormat("dd MMMM yyyy") which is "08 July 2015".
	 * @return
	 */
	public static String getDate2ddMMMMYYY(Date date) {
		if(date==null)
			date = new Date();
	    SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
	    return  sdf.format(date);
	}
	/**
	 * This method will returns the current Date in SimpleDateFormat("dd MMMM yyyy") which is "08 July 2015".
	 * @return
	 */
	public static String getDate2ddMMMMYYY() {
	    Date date = new Date();
	    return  getDate2ddMMMMYYY(date);
	}
	
	/**
	 * This method will returns the Date object takes the parameter as "dd MMMM yyyy" which is "08 July 2015".
	 * @return
	 * @throws ParseException 
	 */
	public static Date getddMMMMYYY2Date(String ddMMMMyyyy) throws ParseException {
	    SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
	    return  sdf.parse(ddMMMMyyyy);
	}
	public static void main(String[] arg){
		Calendar cal = Calendar.getInstance();
		String endDate = GenieDate.getDate2ddMMMMYYY(cal.getTime());
		cal.add(Calendar.MONTH, -1);
		String startDate = GenieDate.getDate2ddMMMMYYY(cal.getTime());
		System.out.println("startDate:"+startDate);
		System.out.println("endDate:"+endDate);
		
		String ddMMMMyyyy = getDate2ddMMMMYYY();
		System.out.println("date:"+ddMMMMyyyy);
		try {
			Date dt = getddMMMMYYY2Date(ddMMMMyyyy);
			System.out.println("dt.toString()="+dt.toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Generate timestamp.
	 *
	 * @param timestamp the timestamp
	 * @return the string
	 */
	public static String  generateTimestamp(Timestamp timestamp) {
		if (timestamp == null)
			return "";
		Calendar calendar =  Calendar.getInstance();
		calendar.setTimeInMillis( timestamp.getTime() );
		return generateTimestamp(calendar);
	}
	
	/**
	 * Generate timestamp.
	 *
	 * @param calendar the calendar
	 * @return the string
	 */
	public static String  generateTimestamp(Calendar calendar) {
	    int year = calendar.get(Calendar.YEAR);
	    int month = calendar.get(Calendar.MONTH) + 1;
	    int day = calendar.get(Calendar.DAY_OF_MONTH);
	    int hour = calendar.get(Calendar.HOUR_OF_DAY);
	    int minutes = calendar.get(Calendar.MINUTE);
	    int seconds = calendar.get(Calendar.SECOND);

	    StringBuffer buffer = new StringBuffer();

	    if (year < 1000) {
	        buffer.append('0');
	        if (year < 100) {
	            buffer.append('0');
	            if (year < 10) {
	                buffer.append('0');
	            }
	        }
	    }
	    buffer.append(Integer.toString(year));
	    buffer.append('-');

	    if (month < 10) {
	        buffer.append('0');
	    }
	    buffer.append(Integer.toString(month));
	    buffer.append('-');

	    if (day < 10) {
	        buffer.append('0');
	    }
	    buffer.append(Integer.toString(day));
	    buffer.append(' ');

	    if (hour < 10) {
	        buffer.append('0');
	    }
	    buffer.append(Integer.toString(hour));
	    buffer.append(":");

	    if (minutes < 10) {
	        buffer.append('0');
	    }
	    buffer.append(Integer.toString(minutes));
        buffer.append(":");

	    if (seconds < 10) {
	        buffer.append('0');
	        
	    }
	    buffer.append(Integer.toString(seconds));

	    return buffer.toString();
	}
	public static String getHHMMSS(Calendar earlierDate,Calendar laterDate){
		return getHHMMSS(laterDate.getTime(), laterDate.getTime());
	}
	public static String getHHMMSS(Date earlierDate,Date laterDate){
		long totalMilliSececonds=laterDate.getTime() - earlierDate.getTime();
		int totalSececonds = (int) (totalMilliSececonds/1000);
		int seconds = totalSececonds%60;
		int totalMinutes = totalSececonds/60;
		int minutes = totalMinutes%60;
		int totalHours = totalMinutes/60;
		String sessionDuration =  totalHours+":"+minutes+":"+seconds;
		System.out.println("sessionDuration=" + sessionDuration);
		return sessionDuration;
	}
}
