-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 26, 2015 at 11:36 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `globalvodaphone`
--

-- --------------------------------------------------------

--
-- Table structure for table `chatglobalcase`
--

CREATE TABLE IF NOT EXISTS `chatglobalcase` (
  `nGlobalCaseID` int(11) NOT NULL,
  `CustName` varchar(100) NOT NULL DEFAULT 'NA',
  `nWaitTime` int(9) NOT NULL,
  `dtSessionStartTime` datetime NOT NULL,
  `dtEndTime` datetime NOT NULL,
  `AgentName` varchar(100) NOT NULL,
  `message` longtext NOT NULL,
  `sessionTime` int(11) DEFAULT NULL,
  `AgentKeywords` mediumtext,
  `Postpaid` tinyint(4) DEFAULT NULL,
  `Prepaid` tinyint(4) DEFAULT NULL,
  `Sentiment` tinyint(4) DEFAULT NULL,
  `CustKeywords` mediumtext,
  `IP` varchar(45) DEFAULT NULL,
  `Location` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Session table to hold the header information';

-- --------------------------------------------------------

--
-- Table structure for table `chatmsgcontent`
--

CREATE TABLE IF NOT EXISTS `chatmsgcontent` (
`ID` int(11) NOT NULL,
  `nGlobalCaseID` int(11) NOT NULL,
  `msgTime` datetime NOT NULL,
  `speaker` char(2) CHARACTER SET utf8 NOT NULL,
  `msg` longtext NOT NULL,
  `MsgKeywords` tinytext CHARACTER SET utf8,
  `SpeakerName` varchar(100) NOT NULL,
  `QRT` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chatmsgcontent`
--
ALTER TABLE `chatmsgcontent`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chatmsgcontent`
--
ALTER TABLE `chatmsgcontent`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
